DESTDIR ?= /
OUT = pam_whale.py

all: $(OUT)

install:
	@mkdir -p $(DESTDIR)/lib/security
	install -o0 -g0 -m644 $(OUT) $(DESTDIR)/lib/security
